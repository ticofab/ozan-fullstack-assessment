## What we expect from this test:
* It has to work! :)
* Over-engineering is not allowed!
* Make the product owner "wow!" with a cool frontend and the engineers "wow!" with cool tests!
* Have fun and learn something new!
* Use ExpressJS and MongoDB
* Use React server side rendering, here we use `http://fluxible.io/`, but if you know something better show us off. 

## Installation

### Prerequesites
  node version: 5.5.0
  A local MongoDB server (^3.4)

### Install all dependencies
```bash
  npm install
```

### First start a mongodb instance in a new tab
```bash
  sudo mkdir -p /data/db
  sudo chown `whoami` -r /data
  mongod
```

### Start a local client
```bash
  npm run start
```

Test
----------

### All tests should be implemented and should be running green
```bash
  npm run test
```

### Single tests -> run all with `npm run all-tests`
* `node_modules/.bin/mocha server/test/utils.spec.js`
* `node_modules/.bin/mocha server/utils/api-utils.spec.js`
* `node_modules/.bin/mocha server/api/user/user.spec.auth.js`
* `node_modules/.bin/mocha lib/model/models/authCode/authcode.model.spec.js`
* `node_modules/.bin/mocha lib/model/models/user/user.model.spec.js `

User stories
--------------

See [USER_STORIES](USER_STORIES.md)