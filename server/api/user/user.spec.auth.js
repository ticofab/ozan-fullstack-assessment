'use strict'

let app = require('../../app')

require('should')
let request = require('supertest')

let utils = require('../../test/utils')
let testAuth = require('../../test/auth')

let Model = require('model')
let User = Model.User

testAuth({
  name: '[Auth] Users: ',
  models: [User],
  endpoints: [
    {
      description: 'GET api/users',
      req: function () {
        return request(app).get('/api/users')
      }
    },
    // /* Putting a user is not secured. Everyone can sign up.
    {
      description: 'PUT api/users',
      req: function () {
          return request(app).put('/api/users')
      }
    },
    {
      description: 'GET api/users/:id',
      req: function () {
        return request(app).get('/api/users/' + utils.ID)
      }
    },
    {
      description: 'PUT api/users/:id',
      req: function () {
        return request(app).put('/api/users/1')
      }
    },
    {
      description: 'GET api/users/me',
      req: function () {
        return request(app).get('/api/users/me')
      }
    },
    {
      description: 'PUT api/users/me',
      req: function () {
        return request(app).put('/api/users/me')
      }
    },
    {
      description: 'DELETE api/users/:id',
      req: function () {
        return request(app).delete('/api/users/' + utils.ID)
      }
    },
    {
      description: 'GET api/users/toggleOperatorMode',
      req: function () {
        return request(app).put('/api/users/toggleOperatorMode')
      }
    }
  ]
})
