'use strict'

let express = require('express')
let controller = require('./user.controller')
let auth = require('../../auth/auth.service')

let router = express.Router()

/**
 * @api {get} /users showAll
 * @apiVersion 1.0.0
 * @apiName GetUsers
 * @apiGroup Users
 *
 * @apiDescription List all users
 *
 *
 * @apiExample Example usage:
 * curl -i http://localhost/api/users
 *
 * @apiSuccess {Object[]} users
 */
router.get('/', auth.hasRole('admin'), controller.index)

/**
 * @api {put} /users create
 * @apiVersion 1.0.0
 * @apiName CreateUser
 * @apiGroup Users
 *
 * @apiDescription Creates a new user and returns a temporary token that can be used to authenticate that user.
 * Only name, email and password are required. The rest of the profile can be filled in in a later stage.
 *
 * @apiParam {String} phoneNumber   required
 * @apiParam {String} password      required
 * @apiParam {String} email
 *
 * @apiExample Example usage:
 * curl -X PUT -i http://localhost/api/users -d '{
 *    name: 'UserName',
 *    email: 'some.user@test.io',
 *    password: 'plain-text-password'
 * }'
 *
 * @apiSuccess {String} token
 */
router.put('/', controller.create)
router.post('/', controller.create)

/**
 * @api {get} /users/me showLoggedInUser
 * @apiVersion 1.0.0
 * @apiName GetMe
 * @apiGroup Users
 *
 * @apiDescription Shows fetches the user data of the person currently logged in.
 *
 *
 * @apiExample Example usage:
 * curl -i http://localhost/api/users/me
 *
 * @apiSuccess {String} _id
 * @apiSuccess {String} name
 * @apiSuccess {String} email
 */
router.get('/me', auth.isAuthenticated(), controller.me)

/**
 * @api {put} /users/me updateMyself
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup Users
 *
 * @apiDescription Update the logged in user
 *
 * @apiExample Example usage:
 * curl -X PUT -i http://localhost/api/users/me
 *             -d { name: 'hashedPw' }
 *
 */
router.put('/me', auth.isAuthenticated(), controller.updateMyself)

router.get('/availability', auth.isAuthenticated(), controller.getAvailability)

/**
 * @api {put} /users/toggleOperatorMode toggleOperatorMode
 * @apiVersion 1.0.0
 * @apiName ToggleOperatorMode
 * @apiGroup Users
 *
 * @apiDescription Toggle whether a user can act as operator
 *
 *
 * @apiExample Example usage:
 * curl -X PUT -i http://localhost/api/users/toggleOperatorMode
 *
 * @apiSuccess {Boolean} isOperator
 */
router.put('/toggleOperatorMode', auth.isAuthenticated(), controller.toggleOperatorMode)

/**
 * @api {get} /users/:userId show
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup Users
 *
 * @apiDescription Fetches a specific user.
 *
 * @apiParam {String} userId the user id to fetch
 *
 * @apiExample Example usage:
 * curl -i http://localhost/api/users/gHRSGsdfgSDFsdfgSDgsdfHJJTREC
 *
 * @apiSuccess {String} _id
 * @apiSuccess {String} name
 * @apiSuccess {String} email
 */
router.get('/:userId', auth.isAuthenticated(), controller.show)

/**
 * @api {delete} /users/:userId destroy
 * @apiVersion 1.0.0
 * @apiName DeleteUser
 * @apiGroup Users
 *
 * @apiDescription Users are never deleted. Their profiles are merely set to inactive.
 *
 * @apiParam {String} userId id of the user to 'delete'
 *
 * @apiExample Example usage:
 * curl -X DELETE -i http://localhost/api/users/gHRSGsdfgSDFsdfgSDgsdfHJJTRE
 *
 * @apiSuccess {String} _id
 */
router.delete('/:userId', auth.hasRole('admin'), controller.destroy)

/**
 * @api {put} /users/:id update
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup Users
 *
 * @apiDescription Update the user
 *
 * @apiParam {String} id id of the user
 *
 * @apiExample Example usage:
 * curl -X PUT -i http://localhost/api/users/gHRSGsdfgSDFsdfgSDgsdfHJJTRE
 *             -d { name: 'hashedPw' }
 *
 */
router.put('/:id', auth.isAuthenticated(), controller.update)

/**
 * @api {put} /users/:userId/password changePassword
 * @apiVersion 1.0.0
 * @apiName ChangePassword
 * @apiGroup Users
 *
 * @apiDescription Change the user password
 *
 * @apiParam {String} userId id of the user
 *
 * @apiExample Example usage:
 * curl -X PUT -i http://localhost/api/users/gHRSGsdfgSDFsdfgSDgsdfHJJTRE/password
 *             -d { oldPassword: 'hashedPw', newPassword: 'hashedPw2' }
 *
 */
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword)

module.exports = router
