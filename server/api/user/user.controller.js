'use strict'

let moment = require('moment-timezone')
let later = require('later')

let config = require('../../../lib/config')
let Errors = require('errors')
let jwt = require('jsonwebtoken')
let _ = require('underscore')
let utils = require('../../utils/api-utils')

let UserService = require('./user.service')

// Returns a promise callback that can send a HTTP response
function success(res, statuscode) {
    return function (returnvalue) {
        if (res) {
            res.status(statuscode || 200).json(returnvalue)
        }
    }
}

// -- Controller Methods ---
/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res, next) {
    UserService
        .find(req.query || {}, '-salt -hashedPassword')
        .then(success(res), next)
}

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {

    let userData = req.body
    console.log("exports.create -> ",userData)
    let phoneNumber = userData.phoneNumber
    if (phoneNumber) {
        // Parse phone Number
        phoneNumber = phoneNumber.replace(/\W/g, '')
        if (phoneNumber.indexOf('00') === 0) {
            phoneNumber = phoneNumber.substr(2)
        }
        userData.phoneNumber = utils.parsePhoneNumber(phoneNumber)
    }

    UserService
        .create(req.body)
        .then(function (user) {
            console.error('User saved')
            let token = jwt.sign({_id: user._id}, config.secrets.session, {
                expiresInMinutes: 60 * 5
            })
            return {token: token, _id: user._id}
        })
        .done(success(res), next)
}

/**
 * Get my info
 */
exports.me = function (req, res, next) {
    let userId = req.user._id
    UserService
        .findById(userId)
        .done(success(res), next)
}

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
    let userId = req.params.userId

    UserService
        .findById(userId)
        .then(function (user) {
            if (!user) {
                throw new Errors[404]('User not found')
            }
            return req.user.role === 'admin' ? _.omit(user, 'password') : user.profile
            // return user.profile
        })
        .done(success(res), next)
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res, next) {
    UserService
        .destroy(req.params.id)
        .done(success(res, 204), next)
}

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
    let userId = req.user._id
    let oldPass = String(req.body.oldPassword)
    let newPass = String(req.body.newPassword)

//  if (newPass === undefined) return res.status(300).json({error: 'Password required'})

    UserService
        .changePassword(userId, oldPass, newPass)
        .done(success(res, 204), next)
}

/**
 * Toggle operator mode
 */
exports.toggleOperatorMode = function (req, res, next) {
    let userId = req.user._id
    UserService.findById(userId)
        .then(function (user) {
            user.isOperator = !user.isOperator
            return user.save()
        })
        .then(function (user) {
            return user.profile
        })
        .done(success(res), next)
}

/**
 * Update a user
 */
exports.update = function (req, res, next) {
    let userId = req.params.id
    let userData = req.body
    //  if (newPass === undefined) return res.status(300).json({error: 'Password required'})

    UserService
        .safeUpdate(userId, userData)
        .done(success(res, 200), next)
}

/**
 * Update a user
 */
exports.updateMyself = function (req, res, next) {
    let userId = req.user._id
    let userData = req.body
    //  if (newPass === undefined) return res.status(300).json({error: 'Password required'})

    UserService
        .safeUpdate(userId, userData)
        .done(success(res, 200), next)
}

/**
 * Get user availability
 */
exports.getAvailability = function (req, res, next) {
    let userId = req.user._id
    //  if (newPass === undefined) return res.status(300).json({error: 'Password required'})

    UserService
        .findById(userId)
        .then(function (user) {
            let availability = []

            if (!user.schedule && !user.schedule.schedules) {
                return availability
            }
            user = user.toObject()
            user.schedule.schedules.map(function (schedule) {
                delete schedule._id
                delete schedule.from
                delete schedule.to
                return schedule
            })
            console.error(user.schedule)
            return later.schedule(user.schedule).nextRange(1000, moment().toDate(), moment().add(1, 'months').toDate())
        })
        .done(success(res), next)
}

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
    res.redirect('/')
}
