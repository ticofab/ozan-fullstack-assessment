/**
 * Express configuration
 */

'use strict'
let logger = require('logger')
let express = require('express')
let morgan = require('morgan')
let compression = require('compression')
let bodyParser = require('body-parser')
let methodOverride = require('method-override')
let cookieParser = require('cookie-parser')
let path = require('path')
// let lusca = require('lusca')

let config = require('../../lib/config')
let passport = require('passport')
let session = require('express-session')
let connectMongo = require('connect-mongo')
let mongoose = require('mongoose')

let MongoStore = connectMongo(session)

let cors = require('cors')

module.exports = function (app) {
    let env = app.get('env')

    app.set('views', __dirname + '/../views');

    //TODO: Replace engine EJS and work towards having it with REACT
    app.set('view engine', 'ejs');


    app.set('view options', {layout: false});
    app.use(compression())
    app.use(bodyParser.urlencoded({limit: '15mb', extended: false}))
    app.use(bodyParser.json({limit: '15mb'}))
    app.use(methodOverride())
    app.use(cookieParser())
    app.use(cors())
    app.use(passport.initialize())
    app.use(passport.session());

    // Persist sessions with mongoStore / sequelizeStore
    // We need to enable sessions for passport-twitter because it's an
    // oauth 1.0 strategy, and Lusca depends on sessions

    app.use(session({
        secret: config.secrets.session,
        saveUninitialized: true,
        resave: false,
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            db: config.mongo.db
        })
    }))

    /**
     * Lusca - express server security
     * https://github.com/krakenjs/lusca

     if ('test' !== env) {
    app.use(lusca({
      csrf: {
        angular: true
      },
      xframe: 'SAMEORIGIN',
      hsts: {
        maxAge: 31536000, //1 year, in seconds
        includeSubDomains: true,
        preload: true
      },
      xssProtection: true
    }))
  }
     */

    // app.use(morgan('dev'))
    app.set('appPath', config.root + '/public')
    if (env === 'production') {
        app.use(express.static(app.get('appPath')))
        app.use(morgan('combined', {stream: logger.stream}))
    }

    if (env === 'development' || env === 'test') {
        app.use(require('connect-livereload')())

        app.use(express.static(path.join(config.root, '.tmp')))
        app.use(express.static(app.get('appPath')))
        app.use(morgan('dev', {stream: logger.stream}))
    }
}
