'use strict'

let utils = require('./utils')

/**
 * @module test/auth
 * @param {Object} settings   Object containing endpoints
 * @description This is a Utility module to test authorization on multiple
 *  endpoints.<br>
 *  It tests 3 situations:<br>
 *  <ol>
 *    <li> [401] Attempt to access the endpoint without token.</li>
 *    <li> [401] Attempt to access the endpoint with an invalid token.</li>
 *    <li> [!401] Attempt to access the endpoint with valid token.</li>
 *  </ol>
 *
 *	It accepts a settings object, describing the endpoints:
 *	@example
 *	let app = require('../../app.js')
 *	let request = require('supertest')
 *	let Model = require('model')
 *	let authTest = require('./test/auth')
 *
 *	authTest({
 *		name: 'Users',
 *		models: [Model.User],
 *		endpoints: [
 *			{
 *				description: 'GET /api/users',
 *				req: function () {
 *					return request(app).get('/api/users')
 *   			}
 *			}
 *		]
 *	})
 *
 */
module.exports = function authTest(settings) {
  describe("Auth Tests ["+settings.name+"] ", function () {
    let token = null

    // Clear DB before testing
    before(function (done) {
      utils
        .resetDB(settings.models)
        .then(function (userToken) {
          // Set auth token
          token = userToken
          done()
        }).catch(done)
    })

    // Cleanup after test
    after(function (done) {
      utils.dropDB(settings.models).then(function () {
        done()
      }).catch(done)
    })

    // The actual test set
    settings.endpoints.forEach(function testAuth (route) {
      describe(route.description, function () {
        let req = route.req

        xit('should return a 401 Unauthorized when not logged in', function (done) {
          testLogin(req, done)
        })

        xit('should return a 401 Unauthorized when using an invalid token', function (done) {
          testToken(req, done)
        })

        xit('should not return a 401 when a user is authorized', function (done) {
          testAuthorized(req, done)
        })
      })
    })

    /**
     * Tests whether a call without token is blocked.
     * Expects a 401 Unauthorized.
     *
     * @param  {Object}   baseRequest Request object to fire
     * @param  {Function} done        Callback
     */
    function testLogin (baseRequest, done) {
      baseRequest()
        .set('Accept', 'application/json')
        .expect(401)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }

          res.body.should.have.properties({message: 'No authorization token was found'})

          done()
        })
    }

    /**
     * Tests whether a call with invalid token is blocked.
     * Expects a 401 Unauthorized.
     *
     * @param  {Object}   baseRequest Request object to fire
     * @param  {Function} done        Callback
     */
    function testToken (baseRequest, done) {
      baseRequest()
        .set('Accept', 'application/json')
        .expect(401)
        .query({'access_token': token + 'a'})
        .end(function (err, res) {
          if (err) {
            return done(err)
          }

          res.body.should.have.properties({message: 'invalid signature'})

          done()
        })
    }

    /**
     * Tests whether a call with a valid token passes.
     * Expects anything but a 401 Unauthorized.
     *
     * @param  {Object}   baseRequest Request object to fire
     * @param  {Function} done        Callback
     */
    function testAuthorized (baseRequest, done) {
      baseRequest()
        .set('Accept', 'application/json')
        .query({'access_token': token})
        .end(function (err, res) {
          if (err) {
            return done(err)
          }

          res.statusCode.should.not.equal(401)

          done()
        })
    }
  })
}
