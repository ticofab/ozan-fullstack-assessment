'use strict'
require('../app')
let should = require('should')
let Model = require('../../lib/model')
let User = Model.User
let UserMock = User._mocks
let utils = require('./utils')

describe('model.find', () => {
  xit('should succeed', function (done) {
    User.find().then(function (users) {
      console.error('GOT HERE')
      should.exist(users)
      done()
    }).catch(done)
  })
})
describe('model.create', () => {
  xit('should succeed saving a minimal mock object', function (done) {
    let userData = UserMock.getMinimal()
    User.create(userData).then(function (user) {
      should.exist(user)
      done()
    }).catch(done)
  })
})

describe('model.remove', () => {
  xit('should work', (done) => {
    User.remove({}).then((res) => done()).catch(done)
  })
})
describe('dropDB', () => {
  xit('should work', (done) => {
    utils.dropDB([User]).then(() => done()).catch(done)
  })
})
