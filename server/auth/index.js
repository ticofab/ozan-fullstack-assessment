'use strict'

let express = require('express')
let config = require('config')

let Model = require('../../lib/model')
let User = Model.User
let AuthCode = Model.AuthCode

let router = express.Router()

// Passport Configuration
require('./local/passport').setup(User, config)
router.use('/local', require('./local'))

//TODO: Implement UserStory:
// # `As a user, I want to receive a unique code in my email, so I can log in a more securely`
require('./passcode/passport').setup(User, AuthCode, config)
router.use('/passcode', require('./passcode'))

module.exports = router
