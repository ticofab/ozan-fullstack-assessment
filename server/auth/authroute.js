'use strict'

//TODO: Hi there developer, implement here your endpoints for login,logout,passcode and reset, This is part of your task on two-step verification process.

module.exports = function (app, passport, User, AuthCode) {
    // Initialize the data


    app.get('/login', function (req, res) {
        res.render(
            'index',
            {
                isAuthenticated: req.isAuthenticated(),
            }
        );
    });

    app.post('/logout', function (req, res) {

    });

    /* Token key is generated and sent to the email address provided by the user. */
    app.post('/passcode', function (req, res) {

    });

    app.post('/reset', function (req, res) {

    });
};
