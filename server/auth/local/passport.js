'use strict'

let passport = require('passport')
let LocalStrategy = require('passport-local').Strategy
let utils = require('../../utils/api-utils')

function localAuthenticate(User, username, password, done) {
    console.log("*&*&*&* localAuthenticate -> ",username)
    User
        .findOne({username: username.toLowerCase()})
        .then(function (user) {
            if (!user) {
                console.log("*&*&*&* Error: Could not find user -> ", username, password)
                return done(null, false, {
                    message: 'This username is not registered.'
                })
            }
            user.authenticate(password, function (authError, authenticated) {
                if (authError) {
                    console.log("*&*&*&* Error: Could not authenticate user -> ", username, password)
                    return done(authError)
                }
                if (!authenticated) {
                    console.log("*&*&*&* Error: This password is not correct. -> ", username, password)
                    return done(null, false, {
                        message: 'This password is not correct.'
                    })
                } else {
                    return done(null, user)
                }
            })
        })
        .catch(function (err) {
            console.log("*&*&*&* Error: catch -> ", username, password, err)
            return done(err)
        })
}

exports.setup = function (User, config) {
    passport.use('local', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password' // this is the virtual field on the model
    }, function (username, password, done) {
        return localAuthenticate(User, username, password, done)
    }))
}
