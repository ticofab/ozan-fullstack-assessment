'use strict'
require('logger')

let util = require('util')

let messages = {
  global: {
    hello: 'hello %s',
    after: 'na',
    tomorrow: 'morgen',
    today: 'vandaag'
  }
}

function getDescendantProp (obj, desc) {
  let path = desc.split('.')

  while (path.length && obj) {
    obj = obj[path.shift()]
  }
  return obj
}

class I18n {
  constructor () {
    this.messages = messages
  }

  get () {
    // Get arguments array
    let $_len = arguments.length
    let args = new Array($_len)
    for (let $_i = 0; $_i < $_len; $_i += 1) {
      args[$_i] = arguments[$_i]
    }

    let key = args.shift()
    let message = getDescendantProp(this.messages, key)

    args.unshift(message)

    return util.format.apply(this, args)
  }
}

let i18n = new I18n()
module.exports = i18n
