'use strict'

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    db: 'prototype-dev'
  },

  // seedDB: false,
  log: 'debug'
}
