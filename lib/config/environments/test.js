'use strict'

// Test specific configuration
// ===========================
module.exports = {
  // MongoDB connection options
  mongo: {
    db: 'prototype-test',
    uri: 'mongodb://localhost'
  },

  seedDB: process.env.SEED || false,

  secrets: {
    session: "verysecret",
  }



}
