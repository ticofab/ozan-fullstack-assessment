'use strict'

let _ = require('underscore')
let Chance = require('chance')

const INVALID_ID = '000000000000000000000000'

function generatePhoneNumber() {
    let chance = new Chance()
    return '32485' + chance.string({length: 6, pool: '0123456789'})
}

function getMinimal(fixedData) {
    let chance = new Chance()
    let returnValue = {
        _id: chance.hash({length: 24}),
        phoneNumber: generatePhoneNumber(),
        username: chance.string({length: 4, pool: '1234567890'}),
        passcode: chance.string({length: 4, pool: '1234567890'}),
        password: chance.string({length: 4, pool: '1234567890'})
    }

    return _.assign(returnValue, fixedData)
}

function get(fixedData) {
    return getMinimal(fixedData)
}

exports.INVALID_ID = INVALID_ID
exports.get = get
exports.getMinimal = getMinimal
