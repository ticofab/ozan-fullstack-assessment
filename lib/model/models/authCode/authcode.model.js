'use strict'

let mongoose = require('mongoose') // require('bluebird').promisifyAll(require('mongoose'))
let Schema = mongoose.Schema

let AuthCodeSchema = new Schema({
    deviceId: String,
    username: String,
    password: String,
    phoneNumber: {
        type: String,
        lowercase: true,
        // required: true,
        index: true
    },
    email: String,
    passcode: {type: String, required: true},
    salt: String,
    createdAt: {
        type: Date,
        default: Date.now,
        expires: '15m'
    }
})

// Public profile information
AuthCodeSchema
    .virtual('public')
    .get(function () {
        return {
            deviceId: this.deviceId,
            phoneNumber: this.phoneNumber,
            username: this.username,
            createdAt: this.createdAt
        }
    })

/**
 * Validations
 */

let validatePresenceOf = function (value) {
    return value && value.length
}

/**
 * Pre-save hook
 */
AuthCodeSchema
    .pre('save', function (next) {
        // Handle new/update passwords
        if (this.isModified('passcode')) {
            if (!validatePresenceOf(this.passcode)) {
                next(new Error('Invalid passcode'))
            }
        }
        next()
    })

module.exports = mongoose.model('AuthCode', AuthCodeSchema)
