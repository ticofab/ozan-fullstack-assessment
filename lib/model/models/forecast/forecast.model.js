'use strict'

let crypto = require('crypto')
let mongoose = require('mongoose')
let Schema = mongoose.Schema

var textSearch    = require('mongoose-text-search');

let ForecastSchema = new Schema({
    user: 'String',
    name: 'String',
    description: 'String',
    startDate:  {type: Date, default: Date.now},
    endDate: {type: Date, default: Date.now},
    completed: {
        type: Boolean,
        default: false
    },
    uid: 'String',
    tid: 'String',
    status: 'String',
    counter: {type: Number, default: 0},
});


ForecastSchema.plugin(textSearch);

var Forecast;

module.exports = Forecast =  mongoose.model('Forecast', ForecastSchema);

Forecast.on('index', function (err) {
    // error occurred during index creation
    if (err) { console.error(err); }
});

module.exports = mongoose.model('Forecast', ForecastSchema);
