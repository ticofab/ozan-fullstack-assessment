'use strict'

//TODO: Implement mock for Forecast and use it when testing the model

let _ = require('underscore')
let Chance = require('chance')

const INVALID_ID = '000000000000000000000000'

function generateId() {
    let chance = new Chance()
    return chance.hash({length: 24})
}

function generateString() {
    let chance = new Chance()
    return '32485' + chance.string({length: 6, pool: '0123456789'})
}

function getMinimal(fixedData) {
    let returnValue = {
        name: generateString(),
        description: generateString(),
        startDate: new Date(),
        endDate: new Date() + 86000,
        completed: false,
        tid: generateString(),
        uid: generateString(),
        status: "open",
        counter: 0,
    }
    return _.assign(returnValue, fixedData)
}

function get(fixedData) {
    let userData = {}
    return _.assign(getMinimal(), userData, fixedData)
}

exports.INVALID_ID = INVALID_ID
exports.generateId = generateId
exports.get = get
exports.getMinimal = getMinimal
