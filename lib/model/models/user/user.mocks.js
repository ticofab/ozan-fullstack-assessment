'use strict'

let _ = require('underscore')
let Chance = require('chance')

const INVALID_ID = '000000000000000000000000'

function generateId() {
    let chance = new Chance()
    return chance.hash({length: 24})
}

function generatePhoneNumber() {
    let chance = new Chance()
    return '32485' + chance.string({length: 6, pool: '0123456789'})
}

function getMinimal(fixedData) {
    let chance = new Chance()
    let returnValue = {
        _id: generateId(),
        phoneNumber: generatePhoneNumber(),
        username: generatePhoneNumber(),
        // powers: { canWork: true },
        password: chance.hash()
    }

    return _.assign(returnValue, fixedData)
}

function get(fixedData) {
    let chance = new Chance()

    let gender = chance.gender()
    let first = chance.first({gender: gender})
    let last = chance.last({gender: gender})

    let userData = {
        apnDeviceToken: chance.apple_token(),

        gender: gender,
        firstName: first,
        lastName: last,
        powers: {canWork: true},
        email: [first, '.', last, '@', chance.domain()].join(''),

        /*
         TODO add addresses
         */
        schedule: {
            schedules: [{
                d: [2, 3, 4, 5, 6],
                h: [9, 10, 11, 12, 14, 15, 16, 17]
            }]
        }
    }

    return _.assign(getMinimal(), userData, fixedData)
}

exports.INVALID_ID = INVALID_ID
exports.generateId = generateId
exports.get = get
exports.getMinimal = getMinimal
exports.generatePhoneNumber = generatePhoneNumber
