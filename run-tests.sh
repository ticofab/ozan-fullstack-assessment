#!/usr/bin/env bash

node_modules/.bin/mocha server/test/utils.spec.js;
node_modules/.bin/mocha server/utils/api-utils.spec.js;
node_modules/.bin/mocha server/api/user/user.spec.auth.js;
node_modules/.bin/mocha lib/model/models/authCode/authcode.model.spec.js;
node_modules/.bin/mocha lib/model/models/user/user.model.spec.js